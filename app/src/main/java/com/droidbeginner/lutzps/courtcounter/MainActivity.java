package com.droidbeginner.lutzps.courtcounter;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    int pontosTimeA = 0;
    int pontosTimeB = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void tresTimeA(View view) {
        pontosTimeA = pontosTimeA + 3;
        displayForTeamA(pontosTimeA);

    }

    public void doisTimeA(View view) {
        pontosTimeA = pontosTimeA + 2;
        displayForTeamA(pontosTimeA);

    }

    public void umTimeA(View view) {
        ++pontosTimeA;
        displayForTeamA(pontosTimeA);

    }

    public void tresTimeB(View view) {
        pontosTimeB = pontosTimeB + 3;
        displayForTeamB(pontosTimeB);

    }

    public void doisTimeB(View view) {
        pontosTimeB = pontosTimeB + 2;
        displayForTeamB(pontosTimeB);

    }

    public void umTimeB(View view) {
        ++pontosTimeB;
        displayForTeamB(pontosTimeB);

    }

    public void reset(View view) {
        pontosTimeA = 0;
        pontosTimeB = 0;
        displayForTeamA(pontosTimeA);
        displayForTeamB(pontosTimeB);

    }

    private void displayForTeamA(int pontosTimeA) {
        TextView pontos = (TextView) findViewById(R.id.pontos_time_a);
        pontos.setText(String.valueOf(pontosTimeA));
    }

    private void displayForTeamB(int pontosTimeA) {
        TextView pontos = (TextView) findViewById(R.id.pontos_time_b);
        pontos.setText(String.valueOf(pontosTimeA));
    }
}
